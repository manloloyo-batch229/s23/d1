// console.log("Hello")

// [SECTION] - Objects
/*// SYNTAX: let/const objectName = {
	keyA: valueA,
	keyB: valueB
}*/
// Object example with already had a value
let cellphone = {
	name: "Nokia 3210", //initializers
	manufactureDate: 1999 //initializers
};

console.log("Result from creating objects using initializer/literal notation");
console.log(cellphone);
console.log(typeof cellphone);

// This is an object

function Laptop(name, manufactureDate){
	// this. for empty initialization and giving paramiters or new parameters 
	this.name = name,
	this.manufactureDate = manufactureDate
}

// This is an instance of an object
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating object using object constructors: ");
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating object using object constructors: ");
console.log(myLaptop);

// we cannot create new object without the instance or the "new" keyword
// example 
let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating object using object constructors: ");
console.log(oldLaptop); //it will return undefined value because it doesnt have "new" keyword

// Creating empty objects

let computer = {}
console.log(computer);

let array = [laptop, myLaptop];
console.log(array[0]["name"]); // this is confusing because or array method
console.log(array[0].name); //proper way of accessing object property value is by using ./dot notation
 




 // [SECTION] - Initializing, Deleting, Re-assigning Object Properties

let car = {}

// Adding new key and value (key, value)
// key pairs consist -properties and values
car.name = "Honda Civic";

console.log(car);

// {} - Object Literal - Great for creating dynamic objects
let pokemon1 = {
	name: "Pikachu",
	type: "Electric"
}
let pokemon10 = {
	name: "Mew",
	type: ["Psychic", "Normal"]
}
let pokemon25 = {
	name: "Charizard",
	level:30
}


// example of constructor function and how to use 
//function Laptop(name, manufactureDate){
	// this. for empty initialization and giving paramiters or new parameters 
// 	this.name = name,
// 	this.manufactureDate = manufactureDate
// }

// Constructor function - allows us to create objects with a defined structure

function Pokemon(name,type,level){
	// - keyword used to refer to the object to be created with the constructor function
	this.name = name;
	this.type = type;
	this.level = level;
}

// new keyword allows us to create an instance of an object with a constructor function

let pokemonInstance1 = new Pokemon("Bulbasaur", "Grass", 32);


// Objects can have arrays as properties:

let professor1 = {
	name: "Romenick Garcia",
	age: 25,
	subjects: ["MongoDB", "HTML", "CSS", "JS"]
}

console.log(professor1.subjects);

// What if we want to add an item in the array within the object?
// the .push will add another input string inside the subject value
professor1.subjects.push("NodeJS");
console.log(professor1.subjects);

// Mini-Activity: Display in the console each subject from professor1's subject from professor1's subject array:

// Approach 1 - access the items from the array using its index
console.log(professor1.subjects[0]);
console.log(professor1.subjects[1]);
console.log(professor1.subjects[2]);
console.log(professor1.subjects[3]);
console.log(professor1.subjects[4]);

// Approach 2 - we can also use forEach() to display each item in the array:

professor1.subjects.forEach(subjects => console.log(subjects));
// traditional use of forEach
professor1.subjects.forEach(function(subjects){
	console.log(subjects);
})

let dog1 = {
	name: "Bantay",
	breed: "Golden Retriever"
};
let dog2 = {
	name: "Bolt",
	breed: "Aspin"
};

// Array of objects:

let dogs = [dog1, dog2];

// How can we display the details of the first item in the dogs array?
// You can use the index number of the item to access it from the array:

// Access all the value
console.log(dogs[0]);
// access a specific value of an object using .
console.log(dogs[1].name);
// You can still access using this but it is more confusing 
console.log(dogs[1]["name"]);

// What if we want to delete the second item from the dogs array

dogs.pop();
console.log(dogs);

dogs.push({

	name: "Whitey",
	breed: "Shih Tzu"

})
console.log(dogs[1].name);


// Initialize, delete, update object properties

let supercar1 = {};

console.log(supercar1);

// Initialize/Add properties and values with our empty object:
supercar1.brand = "Porsche";

console.log(supercar1);

supercar1.model = "Porsche 911";
supercar1.price =  182900;

console.log(supercar1);
console.log(supercar1.price);

// Delete object properties with the delete keyword
delete supercar1.price;

console.log(supercar1);

// Update the values of an object using the ./dot notation:

supercar1 = "718 Boxster";

console.log(supercar1);

// Another example on editing value inside
pokemonInstance1.type = "Grass,Normal";
console.log(pokemonInstance1);


// Object Methods are just functions in an object
// This is useful for creating functions that are associated to a specific object
// Object - person1
let person1 = {
	name: "Joji",
	talk: function(){ //a method for the object or in person1
		console.log('Hello');
	}
};

person1.talk();

let person2 = {
	name: "Joe",
	talk: function(){
		console.log("Hello, World!")
	}
}

person2.talk();

person2.walk = function(){
	console.log("Joe has walked 500 miles")
}

person2.walk();

// .this keyword in an object method refers to the current object where the method is.

person1.introduction = function(){
	console.log("Hi! I am " + " " + this.name + "!");

}
person1.introduction();

person2.introduction = function(){
	console.log(this);
}
person2.introduction();

//Mini- Activity
// Create a new object as student1
// The object should have the following properties:
//name, age, address
// Add 3 methods for student1
// introduceName = which will display the student1's name as:
// "Hello! My name is <nameofstudent1>"
// introduceAge = which will display the age of student1 as:
// "Hello! I am <ageofstudent1> years old."
// introduceAddress = which will display the address of student1 as:
// Hello! I am <


// solution
let student1 = {
	name: "Liester",
	age: 25,
	address: "Argao, Cebu",

	introduceName: function() {
		console.log("Hello My name is " + this.name);
	},
	introduceAge: function(){
		console.log("Hello My name is " + this.age + " years old.")
	},
	introduceAddress: function(){
		console.log("Hello I am " + this.name + " . I live in " + this.address);
	}
}

student1.introduceName();
student1.introduceAge();
student1.introduceAddress();


function Student(name,age,address){
	this.name = name;
	this.age = age;
	this.address = address;
// We can also add methods to our constructor function

this.introduceName = function() {
	console.log("Hello! My name is " + this.name);
};
this.introduceAge = function() {
	console.log("Hello I am " + this.age + "years old");
}
this.introduceAddress = function() {
	console.log("Hello I am" + this.name + " . I live in " + this.address);
}
	// We can also add methods that take other objects as an argument
this.greet = function(person){
	// person's/ object's properties are accessible in the method
	console.log("Good day, " + person.name + "!");
}

}

let newStudent1 = new Student("John",25,"Cebu");
console.log(newStudent1);

let newStudent2 = new Student("Jeremiah",26,"Lucena, Quezon");

newStudent2.greet(newStudent1);

// Mini-Activity
// Create a new constructor function called Dog which is able to create objects with the ff. properties:
// name,breed
// The constructor function should also have 2 methods
// call() - is a method which will allow us to display the ff. message:
// "Bark Bark Bark"
// greet() - is a method which allow us to greet another person. This method should be able to receive an object which has a name property. Upon invoking the greet() method it should display:
// "Bark, Bark <nameofPerson"

// Create an instance/object from the Dog constructor
// Invoke its call() method
// Invoke its greet method to greet newStudent2

// Take a screenshot of your output

function Dog(name, breed){
	this.name = name;
	this.breed = breed;

	this.call= function(){
		console.log("Bark bark bark");
	}
	this.greet= function(person){
		// We are assuming that the data passed as a person is an object with a name property
		// Best practice is console.log first the parameter / console.log(person);


		//hasOwnProperty() method will check if the object passed has the indicated property:
		// hasOwnProperty() methos is used on objects and returns a boolean. (True / False) ->output
		//console.log(person.hasOwnProperty("name"));


		console.log("Bark, bark " + person.name);
	}
}
let newDog1 = new Dog("Johny", "Sins");

newDog1.call();

newDog1.greet(newStudent1);
//newDog1.greet(supercar1);
newDog1.greet(newStudent2);





// When creating a function that is supposed to receive data/argument
// It is best practice to log the parameter first
function sample1(sampleParameter){
	//the argument you passed during invocation is now represented by the parameter
	//console.log(sampleParameter.name);
	newDog1.name = "Gary";
}

// Argument is the data passed during function invocation

sample1(newDog1);
console.log(newDog1);
//sample1(25000);